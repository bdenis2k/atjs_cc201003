// Symbols for display
function cardSymbol(suit) {
    switch (suit) {
        case 'CLUBS':
            return '♣️';
        case 'SPADES':
            return '♠️';
        case 'DIAMONDS':
            return '♦️';
        case 'HEARTS':
            return '♥️';
        default:
            break;
    }
}

// Creating the card itself (new)
export function createCleanCard({ value, suit, image }) {
    return {
        value,
        image,
        symbol: cardSymbol(suit),
    };
}

// Checking for color types
export function validateGuess(card, nextGuess) {
    const colors = {
        SPADES: 'black',
        CLUBS: 'black',
        HEARTS: 'red',
        DIAMONDS: 'red',
    };

    const cardColor = colors[card.suit];

    return cardColor == nextGuess;
}

// New deck API
const API = 'https://deckofcardsapi.com/api/deck/new/shuffle/';

// Getting new deck
export function getDeckAPI() {
    return API;
}

// Drawing card API
export function getCardAPI(deckId) {
    return `https://deckofcardsapi.com/api/deck/${ deckId }/draw/?count=1`
}