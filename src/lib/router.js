// Import router functionality
import { createRouter, createWebHistory } from 'vue-router'
// Import file paths
import Home from '@/pages/Home.vue';
import Pokedex from '@/pages/Pokedex.vue';
import Calculator from '@/pages/Calculator.vue';
import Guesser from '@/pages/Guesser.vue';

// Linking routes to files
const routes = [
    { path: '/', component: Home },
    { path: '/pokedex', component: Pokedex },
    { path: '/calculator', component: Calculator },
    { path: '/guesser', component: Guesser }
];

// Creating router instance
// And defining usable paths
const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router