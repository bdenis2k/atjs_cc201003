const height = process.argv[2] || 10;

for(let layer = 1; layer <= height; layer++) {

    const bottomLayerStones = height * 2 - 1;
    const currentLayerStones = layer * 2 - 1;
    const space = (bottomLayerStones - currentLayerStones) / 2

    const stones = layer * 2 - 1;
    console.log(' '.repeat(space) + '#'.repeat(stones) + ' '.repeat(space))

}

